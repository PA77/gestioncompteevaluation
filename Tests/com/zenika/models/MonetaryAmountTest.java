package com.zenika.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MonetaryAmountTest {

    MonetaryAmount monetaryAmount;

    @BeforeEach
    void setUp(){
        monetaryAmount = new MonetaryAmount(250.0, "euros");
    }

    @Test
    void addAmount() {
        monetaryAmount.addAmount(50.0);
        Assertions.assertEquals(300.0, monetaryAmount.getAmount());
    }

    @Test
    void substractAmount() {
        monetaryAmount.substractAmount(100.0);
        Assertions.assertEquals(150.0, monetaryAmount.getAmount());
    }

    @Test
    void testToString() {
        Assertions.assertEquals("Montant : 250.0" + System.lineSeparator() + "Devise : euros" + System.lineSeparator(), monetaryAmount.toString());
    }
}