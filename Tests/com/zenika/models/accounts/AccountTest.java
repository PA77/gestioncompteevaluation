package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;
import com.zenika.models.accounts.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountTest {

    MonetaryAmount monetaryAmount;
    Account account;

    @BeforeEach
        void setUp(){
        monetaryAmount = new MonetaryAmount(200.0, "euros");
        account = new Account("Obélix", monetaryAmount, "euros");
    }

    @Test
    void deposit() {
        account.deposit(5000.0);
        Assertions.assertEquals("Montant : 5200.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), account.getCurrentBalance().toString());
    }

    @Test
    void withdraw() {
        account.withdraw(600.0);
        Assertions.assertEquals("Montant : 200.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), account.getCurrentBalance().toString());
    }

    @Test
    void getOwnerName() {
        Assertions.assertEquals("Obélix", account.getOwnerName());

    }
}