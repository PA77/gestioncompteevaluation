package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrentAccountTest {

    MonetaryAmount monetaryAmount;
    Account account;
    CurrentAccount currentAccount;

    @BeforeEach
    void setUp(){
        monetaryAmount = new MonetaryAmount(200.00, "euros");
        currentAccount = new CurrentAccount("Obélix", monetaryAmount, monetaryAmount.getCurrency());
    }

    @Test
    void withdraw() {
        currentAccount.withdraw(700.00);
        Assertions.assertEquals("Montant : -500.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), currentAccount.getCurrentBalance().toString());

    }

    @Test
    void withdraw2() {
        currentAccount.withdraw(1201.00);
        Assertions.assertEquals("Montant : 200.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), currentAccount.getCurrentBalance().toString());

    }
}