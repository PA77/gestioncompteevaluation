package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SavingAccountTest {

    MonetaryAmount monetaryAmount;
    Account account;
    SavingAccount savingAccount;

    @BeforeEach
    void setUp(){
        monetaryAmount = new MonetaryAmount(200.00, "euros");
        account = new Account("Obélix", monetaryAmount, "euros");
        savingAccount = new SavingAccount("Obélix", monetaryAmount, monetaryAmount.getCurrency(), 0.04);
    }

    @Test
    void withdraw(){
        savingAccount.withdraw(201.00);
        Assertions.assertEquals("Montant : 200.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), savingAccount.getCurrentBalance().toString());
    }

    @Test
    void newCredits() {
        savingAccount.newCredits();
        Assertions.assertEquals("Montant : 208.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), savingAccount.getCurrentBalance().toString());
    }

    @Test
    void newCredits2() {
        monetaryAmount.addAmount(250);
        savingAccount.newCredits();
        Assertions.assertEquals("Montant : 468.0" + System.lineSeparator() + "Devise : euros" +
                System.lineSeparator(), savingAccount.getCurrentBalance().toString());
    }

    //Ceci est le test qui correspond au scénario d'exemple soumis dans la
    //dexième partie de l'évaluation
    @Test
    void scenarioTestPartie2Evaluation(){
        MonetaryAmount mA = new MonetaryAmount(0.0, "Sesterces");
        MonetaryAmount mA2 = new MonetaryAmount(1.0, "Dollars");
        CurrentAccount cA = new CurrentAccount("Obélix", mA, mA.getCurrency());
        SavingAccount sA = new SavingAccount("Obélix", mA2, mA2.getCurrency(), 0.01);


        cA.deposit(100.00);
        System.out.println(cA.getCurrentBalance().toString());

        sA.deposit(30.00);
        System.out.println(sA.getCurrentBalance().toString());

        cA.withdraw(200.00);
        System.out.println(cA.getCurrentBalance().toString());

        sA.newCredits();
        System.out.println(sA.getCurrentBalance().toString());


    }


}