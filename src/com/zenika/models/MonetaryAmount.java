package com.zenika.models;

public class MonetaryAmount {

    //Attributs
    private double amount;
    private String currency;

    //Constructeur de l'objet MonetaryAmount
    public MonetaryAmount(double amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    //Cette méthode ajoute une somme à la quantité d'argent "amount"
    //Elle prend en paramètre un nombre de type primitif double
    public void addAmount(double a){
        this.amount += a;
    }

    //Cette méthode soustrait une somme à la quantité d'argent "amount"
    //Elle prend en paramètre un nombre de type primitif double
    public void substractAmount(double a){
        this.amount -= a;
    }

    @Override
    public String toString() {
        String result = "";
        return result = "Montant : "+ this.amount + System.lineSeparator() +
                "Devise : "+ this.currency + System.lineSeparator();
    }

    //Cette méthode renvoie la quantité d'argent présente dans "amount"
    public double getAmount() {
        return amount;
    }

    //Cette méthode renvoie la devise sous forme de chaîne de caractères
    public String getCurrency() {
        return currency;
    }
}
