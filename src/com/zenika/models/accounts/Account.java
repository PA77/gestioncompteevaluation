package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;

public class Account {

    //Attributs
    MonetaryAmount balance;
    String ownerName;

    //Constructeur
    public Account(String ownerName, MonetaryAmount balance, String currency){
        this.ownerName = ownerName;
        this.balance = balance;
    }

    //Cette méthode permet d'effectuer un dépôt sur le compte
    //Elle appelle la méthode "addAmount" de l'objet balance
    public void deposit(double amount){
       balance.addAmount(amount);
    }


    //Cette méthode permet d'effectuer un retrait sur le compte
    //Elle appelle la méthode "substractAmount" de l'objet balance
    public void withdraw(double amount){
        double res = balance.getAmount();
            if(amount <= res){
                balance.substractAmount(amount);
            }
    }

    //Cette méthode permet d'obtenir la quantité d'argent sur le compte
    public MonetaryAmount getCurrentBalance(){
        MonetaryAmount monetaryAmount = new MonetaryAmount(balance.getAmount(), balance.getCurrency());
         return monetaryAmount;

    }

    //Cette méthode renvoie le nom du possesseur du compte
    public String getOwnerName() {
        return ownerName;
    }
}
