package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;


//Cette classe représente un compte courant
//Elle hérite de la classe "Account"
public class CurrentAccount extends Account {

    private double autorD;

    public CurrentAccount(String ownerName, MonetaryAmount balance, String currency) {
        super(ownerName, balance, currency);
        this.autorD = -1000;
    }

    @Override
    public void deposit(double amount) {
        super.deposit(amount);
    }

    @Override
    public void withdraw(double amount) {
        double solde = balance.getAmount() - amount;

            if(solde >= this.autorD){
                balance.substractAmount(amount);
            } else {
                System.out.println("Cette opération ne peut pas être effectuée");
        }
    }

    @Override
    public MonetaryAmount getCurrentBalance() {
        return super.getCurrentBalance();
    }

    @Override
    public String toString() {
        return super.toString() + "Ceci est un compte Courant ";
    }
}

