package com.zenika.models.accounts;

import com.zenika.models.MonetaryAmount;


//Cette classe représente un compte épargne
//Elle hérite de la classe "Account"
public class SavingAccount extends Account {

    private double txInterest;
    private double initialSum;

    public SavingAccount(String ownerName, MonetaryAmount balance, String currency, double txInterest) {
        super(ownerName, balance, currency);
        this.txInterest = txInterest;
        this.initialSum = 1;

    }

    @Override
    public void deposit(double amount) {
        super.deposit(amount);
    }

    @Override
    public void withdraw(double amount) {
        double solde = balance.getAmount() - amount;
            if(solde > 0){
                balance.substractAmount(amount);
            } else {
                System.out.println("Ce retrait est impossible. Votre solde doit être strictement positif");
            }
    }


    @Override
    public MonetaryAmount getCurrentBalance() {
        return super.getCurrentBalance();
    }

    public void newCredits(){

        double res = balance.getAmount() * this.txInterest;
        balance.addAmount(res);

    }
}
