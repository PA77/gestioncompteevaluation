import com.zenika.models.accounts.Account;
import com.zenika.models.MonetaryAmount;
import com.zenika.models.accounts.CurrentAccount;
import com.zenika.models.accounts.SavingAccount;

public class Main {

    public static void main(String[] args) {


//Etape 1
        MonetaryAmount monetaryAmount = new MonetaryAmount(0, "sesterces");
        Account account = new Account("Obélix",monetaryAmount, monetaryAmount.getCurrency() );

//Etape 2
        System.out.println("Etape 2 : "+"\n");
         System.out.println(String.valueOf(account.getCurrentBalance()));

//Etape 3
        account.deposit(500.5);

//Etape 4
        System.out.println("Etape 4 : "+"\n");
        System.out.println(String.valueOf(account.getCurrentBalance()));

//Etape 5
        account.withdraw(125.5);

//Etape 6
        System.out.println("Etape 6 : "+"\n");
        System.out.println(String.valueOf(account.getCurrentBalance()));

//Etape 7
        account.withdraw(1337.0);

//Etape 8
        System.out.println("Etape 8 : "+"\n");
        System.out.println(String.valueOf(account.getCurrentBalance()));

        Account currentAccount = new CurrentAccount("Obélix", monetaryAmount, "sesterces");
        System.out.println(currentAccount.toString());


    }
}

